<?php

class UserService{

    private $repository;
    
    public function __construct(){
        require '/home/mati/Desktop/php_camp4/Repositories/UserRepository.php';
        $this->repository = new UserRepository();
    }

    public function getAllUsers(): array
    {
        return $this->repository->getAllUsers();
    }

    public function getUserById(int $userId): array
    {
        $arrayId = $this->determineArrayAddressByUserId($userId);
        return $this->repository->getUserAddress($arrayId);
    }

    public function updateUser(int $userId, array $dataArray) :bool {
        $users = $this->getAllUsers();
        $arrayId = $this->determineArrayAddressByUserId($userId);
        foreach($dataArray as $key => $value)
            $users[$arrayId][$key] = $value;
        $usersData = json_encode($users);
        return $this->repository->saveUserData($usersData);
    }

    private function determineArrayAddressByUserId($userId){
        $users = $this->getAllUsers();
        foreach($users as $address => $values){
            if ($values['id'] === $userId)
            {
                $arrayId = $address;
            break;
            }
        }
        return $arrayId;
    }
}