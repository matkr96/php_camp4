<?php
new LogInController();

class LogInController{
    private $userService;

    public function __construct()
    {
        require_once('/home/mati/Desktop/php_camp4/Services/UserService.php');
        require_once('/home/mati/Desktop/php_camp4/init.php');
        $this->userService = new UserService();
        $this->validateUser();
    }

    public function validateUser(){
        if(!empty($_POST['logout']))
            $this->logOutUser();
        elseif(!empty($_POST['login']) && !empty($_POST['password']) && empty($_SESSION['userId']) )
        {
            $login = $_POST['login'];
            $password = $_POST['password'];
            $this->logInUser($login, $password);
        }
        elseif(!empty($_SESSION['userId']))
        {
            $this->redirectUser('/View/main.php');
        }
        else{
            $this->redirectUser('View/LogInForm.php', 'Please fill in login and passwords fields.', 'bad');
        }
    }
    private function redirectUser(string $whereTo, ?string $message = null, string $color='good'){
         if(isset($message))  
            $_SESSION['errorMessage'] = $message;
        switch ($color){
            case 'good':
                $_SESSION['errorMessageColor'] = 'green';
                 break;
            case 'bad':
                $_SESSION['errorMessageColor'] = 'red';
                break;
            default :
                $_SESSION['errorMessageColor'] = 'black';
            }
        return header("Location: $whereTo");
    }

    private function logOutUser(){
        unset($_SESSION['userId']);
        $this->redirectUser('View/LogInForm.php', 'Successfully Logged Out', 'good');
    }
    private function logInUser(string $login, string $password)
    {
        $amongUsers = false;
        $users = $this->userService->getAllUsers();
        foreach($users as $user)
        {
            if(($user['login'] === $login || $user['email'] === $login) && $user['password'] === $password)
            {
                $amongUsers = true;
                $userData = $user;
            break;
            }
        }
        if($amongUsers)
        {
         $this->updateUserInfo($userData);
         $_SESSION['userId'] = $user['id'];
        }
         else
         $this->redirectUser('View/LogInForm.php', 'Invalid email or pasword', 'bad');
        

    }

    private function updateUserInfo($user){
        $currentTime =  date('Y-m-d H:i:s');
        $toUpdate = ["lastLogin" => $currentTime];
        if($this->userService->updateUser($user['id'], $toUpdate))
            $this->redirectUser('View/main.php');
        else{
            file_put_contents('/home/mati/Desktop/php_camp4/logs.txt', 'Cannot update user'.$user, FILE_APPEND);
            $this->redirectUser('View/main.php');
        }
    }

}








?>