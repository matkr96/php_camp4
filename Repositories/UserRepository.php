<?php

class UserRepository{

    private $usersFile;
    
    public function __construct(){
        $this->usersFile = file_get_contents('/home/mati/Desktop/php_camp4/users.json');
    }

    public function getAllUsers() :array
    {
        return  json_decode($this->usersFile, true);
    }   
    
    public function getUserAddress(int $address) :array
    {
        return  json_decode($this->usersFile, true)[$address];
    }

    
    public function saveUserData(string $usersData) :bool{
        return $this->usersFile = file_put_contents('users.json',$usersData);
    }
}