<?php

require_once('/home/mati/Desktop/php_camp4/Utils/Utils.php');
require_once('/home/mati/Desktop/php_camp4/Services/UserService.php');
require_once('/home/mati/Desktop/php_camp4/init.php');
$utils = new Utils;
$utils::isLoggedIn();
$userService = new UserService;
$user = $userService->getUserById($_SESSION['userId']);
$userInfo = '';
foreach ($user as $key => $value)
{
    $userInfo .= "<br/>".$key.': '.$value;
}
?>
<html>
    <head>
        <style>
            #container{
                text-align: left;
                margin-top: 10%;
                margin-left: auto;
                margin-right: auto;
                width: 350px
            }
            </style>
    </head>
    <body>
        <div id="container">
        <?php echo $userInfo; ?>
            <form action="/test.php" method="post">
                <input type="hidden" name="logout" value="logout"/>
                <input type="submit" value = "Log Out">
            </form>
        </div>
    </body>
</html>